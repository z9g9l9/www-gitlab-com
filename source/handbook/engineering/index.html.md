---
layout: markdown_page
title: "Engineering"
---

## Communication<a name="reach-engineering"></a>

- [**Public Issue Tracker (for GitLab CE)**](https://gitlab.com/gitlab-org/gitlab-ce); please use confidential issues for topics that should only be visible to team members at GitLab.
- [**Chat channel**](https://gitlab.slack.com/archives/development); please use the `#development` , `#frontend`, `#infrastructure`, and `#support` chat channels for questions that don't seem appropriate to use the issue tracker or the internal email address for.

## Engineering Groups

- [Support](/handbook/support)
- [Infrastructure](/handbook/infrastructure)
- [Backend]() TODO
- [Frontend]() TODO
- [Packaging]() TODO

## Resources for Engineering

- [Developer onboarding](/handbook/developer-onboarding)
- [Workflow](/handbook/workflow)
