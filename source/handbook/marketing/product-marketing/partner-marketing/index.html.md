---
layout: markdown_page
title: "Partner marketing"
---

Welcome to the Partner Marketing Handbook

[Up one level to the Product Marketing Handbook](/handbook/marketing/product-marketing/)    

## On this page
* [Partner Marketing](#partnermarketing)

## Partner Marketing<a name="partnermarketing"></a>

- Have marque projects move to GitLab by engaging with them, offering support and free licenses.

- Have other tools add support for GitLab.

- Have other tools contribute for their tool to GitLab and have them add themselves on the [application page](https://about.gitlab.com/applications/).

- Companies using GitLab as part of their application such as O'Reilly and Perforce.

- Join with companies that use GitLab and do joint events, have them talk about how they use it, recruiting opportunity for our partners, maybe hire people from partners part time to explain GitLab to others.

- Intercom developer evangelism is great because they get know people to contribute.

- [Strategic Partner listing](https://docs.google.com/document/d/1-oAf0tMlTrAaPAsG_8NLXrI3DEZqI5ZA0gW0lKxFjA4/edit) (internal)
